const functions = require('firebase-functions');

exports.getServerTime = functions.https.onCall((data, context) => {
    //server time in seconds
    var date = new Date();
    var time = Math.round(date / 1000);
    var result = {"value" : time}
    return result;
});